import bo.AutorizationUserBO;
import bo.SendMessagePage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import java.io.IOException;

public class GmailSentMessageTest {


//    @DataProvider(name = "users, parallel = true)
//    public static Object[][] credentials() throws IOException, InvalidFormatException {
//
//    }

//    @BeforeClass
//    public static void initDriver(){
//        System.setProperty("webdriver.chrome.driver", "../src/main/resources/chromedriver.exe");
//    }

    @Test()
        public void gmailMessageTest(String login, String password) {

        WebDriver driver = DriverManager.getInstance().getDriver();
        driver.get("https://www.google.com/gmail/");

        AutorizationUserBO authorisationBO = new AutorizationUserBO(driver);
        authorisationBO.LogIn("softats@gmail.com","88wdxterqw");

       SendMessagePage sendMessageBO = new SendMessagePage(driver);
        sendMessageBO.writeMessageAndClose("sofdatc@gmail.com","nsjj");
        sendMessageBO.closeErrorMessage();
        sendMessageBO.isSent("sofdatc@gmail.com","88wdxterqw");
       // Assert.assertTrue(sendMessageBO.isSent());  //checking if the message was sent

        driver.quit();
    }
}
