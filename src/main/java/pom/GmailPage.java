package pom;


import elements.Button;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wrapper.CustomFieldDecorator;

public class GmailPage {
    private WebDriver driver;
    @FindBy(xpath = "//*[contains(@class,'z0')]//div")
    private Button composeButton;

    @FindBy(xpath = "//div[contains(@class,'T-I J-J5-Ji aoO T-I-atl L3')]")
    private Button sendButton;


    @FindBy(xpath = "//textarea[contains(@name,'to')]")
    private Button toTextArea;

    @FindBy(xpath = "//input[contains(@name,'subjectbox')]")
    private WebElement subjectInput;

    @FindBy(xpath = "//div[contains(@class,'Kj-JD-K7 Kj-JD-K7-bsT')]")
    private WebElement errorMessageLocators;

    @FindBy(xpath = "//*[contains(@name,'ok')]")
    private WebElement closeErrorMessageButton;


    public GmailPage(WebDriver driver) {
        PageFactory.initElements(new CustomFieldDecorator(driver), this);
        this.driver = driver;
    }

    public void clickComposeButton() {
        composeButton.click(); }

    public Button getComposeButton() { return composeButton; }

    public void setComposeButton(Button composeButton) {
        this.composeButton = composeButton;
    }

    public Button getSendButton() {
        return sendButton;
    }

    public void setSendButton(Button sendButton) { this.sendButton = sendButton; }

    public Button getToTextArea() { return toTextArea; }

    public void setToTextArea(Button toTextArea) { this.toTextArea = toTextArea;
    }

    public WebElement getSubjectInput() {
        return subjectInput;
    }

    public void setSubjectInput(WebElement subjectInput) {
        this.subjectInput = subjectInput; }

    public WebElement getErrorMessageLocators() { return errorMessageLocators; }

    public void setErrorMessageLocators(WebElement errorMessageLocators) { this.errorMessageLocators = errorMessageLocators; }

    public WebElement getCloseErrorMessageButton() {
        return closeErrorMessageButton;
    }

    public void setCloseErrorMessageButton(WebElement closeErrorMessageButton) { this.closeErrorMessageButton = closeErrorMessageButton; }

    public void writeMessage( String receiver , String message) {

        toTextArea.clear();
        toTextArea.sendKeys(receiver);
        subjectInput.sendKeys(message);
    }

    public void nextSaveMessage() {
        sendButton.doneClick(driver);
    }

    public void closeErrorMessage() {
        if (errorMessageLocators.isDisplayed()) {
            closeErrorMessageButton.click();
        }
    }

}
