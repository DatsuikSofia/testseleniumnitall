package pom;
import elements.Button;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wrapper.CustomFieldDecorator;

public class LoginAndPasswordPage {
    @FindBy(xpath = "//*[@id='identifierId']")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id='identifierNext']")
    private Button nextButton;

    @FindBy(xpath = "//*[contains(@name,'password')]")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[@id='passwordNext']]")
    private Button passwordNextButton;


    public LoginAndPasswordPage(WebDriver driver){
        PageFactory.initElements(new CustomFieldDecorator(driver),this);
    }

    public void typeLoginAndSubmit (WebDriver driver , String login){
        loginInput.sendKeys(login);
        nextButton.doneClick(driver);
    }

    public void typePasswordAndSubmit (WebDriver driver, String password){
        passwordInput.sendKeys(password);
        nextButton.doneClick(driver);
    }


}
