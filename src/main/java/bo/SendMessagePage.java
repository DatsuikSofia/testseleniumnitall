package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.GmailPage;

public class SendMessagePage {
    private static Logger logger = LogManager.getLogger(SendMessagePage.class);
    private WebDriver driver;
    private WebDriverWait wait;
    private GmailPage gmailPage = new GmailPage(driver);

    public SendMessagePage(WebDriver driver) {

        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void writeMessageAndClose(String receiver, String message) {
        gmailPage.clickComposeButton();
        wait.until(ExpectedConditions.visibilityOf(gmailPage.getToTextArea()));  //waiting for closing letter form
        gmailPage.writeMessage(receiver, message);
        gmailPage.nextSaveMessage();
        logger.info("Message has been sent");
    }

    public void closeErrorMessage() {
        gmailPage.closeErrorMessage();
        logger.info("Error message has been closed");
        wait.until(ExpectedConditions.elementToBeClickable(gmailPage.getCloseErrorMessageButton()));  //waiting for opening letter form
    }


    public boolean isSent(String receiver, String message) {
        try {
            wait.until(ExpectedConditions.visibilityOf(gmailPage.getToTextArea()));
            gmailPage.getToTextArea().clear();
            writeMessageAndClose(receiver, message);
            gmailPage.nextSaveMessage();
        } catch (Exception ex) {
            logger.error("Test failed");
            return false;
        }
        return true;

    }

}
