package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.LoginAndPasswordPage;


public class AutorizationUserBO {
    private LoginAndPasswordPage loginAndPasswordPage;

    private static Logger logger = LogManager.getLogger(AutorizationUserBO.class);

    private WebDriver driver;
    private WebDriverWait wait;

    public AutorizationUserBO (WebDriver driver){
      LoginAndPasswordPage loginAndPasswordPage = new LoginAndPasswordPage(driver);
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void LogIn(String login , String password){
        loginAndPasswordPage.typeLoginAndSubmit(driver,login);//waiting for next page
        loginAndPasswordPage.typePasswordAndSubmit(driver,password);
        wait.until(driver1 -> driver1.getCurrentUrl().toLowerCase().contains("inbox")); //waiting for home page

        logger.info("Log in user successful");
    }
}
